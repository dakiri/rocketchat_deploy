Déploiement d'une instance rocketchat minimaliste en version docker.

La version la plus récente de la sauvegarde du répertoire "backup_root" est restaurée.

Variables :
* rocket_chat_version - version de l'image voir ici:  https://hub.docker.com/_/rocket-chat
* root_path: chemin d'installation 
* backup_root: chemin pour dump de mongodb
